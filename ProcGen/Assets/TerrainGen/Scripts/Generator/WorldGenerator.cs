using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

[ExecuteInEditMode]
public class WorldGenerator : MonoBehaviour {

    [Header ("Terrain Params")]
    [SerializeField] int worldSize = 10;

    [Header ("Chunk Data to Use")]
    [SerializeField] ChunkData_SO chunkData;

    /*
        PRIVATE VARIABLES
    */
    [HideInInspector] public string seed = "24.001";
    Dictionary<Vector3Int, Chunk> chunks = new Dictionary<Vector3Int, Chunk> ();

    [Button]
    void Generate () {
        ClearChunks ();

        if (chunkData._useRandomSeed) {
            seed = Time.time.ToString ();
        } else {
            seed = chunkData._seed;
        }

        for (var x = 0; x < worldSize; x++) {
            for (var z = 0; z < worldSize; z++) {
                Vector3Int chunkPos = new Vector3Int (x * GameData.chunkWidth, 0, z * GameData.chunkWidth);
                Chunk newChunk = new Chunk (chunkPos, chunkData, GetRandomOffset ());
                newChunk.chunkObject.transform.SetParent (transform);
                chunks.Add (chunkPos, newChunk);
            }
        }
    }

    [Button]
    void ClearChunks () {
        if (chunks == null) return;
        // foreach (Transform child in transform) {
        //     GameObject.Destroy (child.gameObject);
        // }

        for (int i = transform.childCount - 1; i >= 0; i--) {
            DestroyImmediate (transform.GetChild (i).gameObject);
        }

        chunks.Clear ();
    }

    float GetRandomOffset () {
        System.Random rng = new System.Random (seed.GetHashCode ());
        float generatedOffset = rng.Next (-100, 100);

        return generatedOffset;
    }

}