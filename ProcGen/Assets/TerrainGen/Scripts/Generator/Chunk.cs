using System;
using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class Chunk {

	//TODO no longer monbehaviour, make vars puplic and set from script that generates chunks

	[Header ("Terrain Params")]
	bool isSmooth;

	[Header ("Reduce Verts for Smooth Shading")]
	bool reduceVerts;

	[Header ("Seed Settings")]
	bool useRandomSeed;
	// string seed = "24.001";

	[Header ("Refs")]
	public GameObject chunkObject;

	/*
		Private Variables
	*/
	float generatedOffset = 24.001f;
	float[, , ] terrainMap;
	List<Vector3> vertices = new List<Vector3> ();
	List<int> triangles = new List<int> ();
	MeshFilter meshFilter;
	MeshCollider meshCollider;
	MeshRenderer meshRenderer;
	Vector3Int chunkPosition; //same as the key for the chunk in the chunks dictionary
	ChunkData_SO chunkData;

	/*
		Get Variables From GameData
	*/
	int width { get { return GameData.chunkWidth; } }
	int height { get { return GameData.chunkHeight; } }
	float terrainSurface { get { return GameData.terrainSurface; } }

	// GameData GameData = new GameData ();

	public Chunk (Vector3Int _position, ChunkData_SO _chunkData, float randomOffset) {

		/*
			Set incoming variables
		*/

		isSmooth = _chunkData._isSmooth;
		reduceVerts = _chunkData._reduceVerts;
		useRandomSeed = _chunkData._useRandomSeed;
		generatedOffset = randomOffset;
		chunkData = _chunkData;

		/*
			Do Constructor Things
		*/

		chunkObject = new GameObject ();
		chunkObject.name = string.Format ("Chunk {0}, {1}", _position.x, _position.z);
		chunkPosition = _position;
		chunkObject.transform.position = chunkPosition;
		meshFilter = chunkObject.AddComponent<MeshFilter> ();
		meshCollider = chunkObject.AddComponent<MeshCollider> ();
		meshRenderer = chunkObject.AddComponent<MeshRenderer> ();
		meshRenderer.material = _chunkData._materialToUse;
		chunkObject.transform.tag = "Terrain";


		//avoid index out of range
		terrainMap = new float[width + 1, height + 1, width + 1];

		//Populate terrain data with Perlin noise - displace 
		PopulateTerrainMap ();
		CreateMeshData ();
	}

	// [Button]
	// void Generate () {
	//avoid index out of range
	// terrainMap = new float[width + 1, height + 1, width + 1];

	// //Populate terrain data with Perlin noise - displace 
	// PopulateTerrainMap ();
	// CreateMeshData ();

	// }

	void CreateMeshData () {

		//Clear any existing data and mesh
		ClearMeshData ();

		for (var x = 0; x < width; x++) {
			for (var y = 0; y < height; y++) {
				for (var z = 0; z < width; z++) {
					//Create cube with new corner data
					MarchCube (new Vector3Int (x, y, z));
					// assetPopulator.PopulateAssetsOnChunk (x, z, GetTerrainDataAtPoint (new Vector3Int (x, y, z)), chunkPosition, generatedOffset, chunkData);

				}
			}
		}

		//Build mesh after mesh data is generated
		BuildMesh ();
	}

	void PopulateTerrainMap () {

		for (var x = 0; x < width + 1; x++) {
			for (var y = 0; y < height + 1; y++) {
				for (var z = 0; z < width + 1; z++) {
					float heightAtPoint;

					heightAtPoint = GameData.GetTerrainHeight (x + chunkPosition.x, z + chunkPosition.z, generatedOffset);

					// Assign point location to 3d array
					terrainMap[x, y, z] = (float) y - heightAtPoint;
				}
			}
		}

	}

	int GetCubeConfig (float[] cube) {
		// Starting with a configuration of zero, loop through each point in the cube and check if it is below the terrain surface.
		int _configIndex = 0;

		for (var i = 0; i < 8; i++) {
			// If it is, use bit-magic to the set the corresponding bit to 1. So if only the 3rd point in the cube was below
			// the surface, the bit would look like 00100000, which represents the integer value 32.
			if (cube[i] > terrainSurface) {
				_configIndex |= 1 << i;

			}
		}
		return _configIndex;
	}

	//TODO iJobFor - Research
	void MarchCube (Vector3Int position) {

		//Array of floats to store the corners of each cube
		float[] cube = new float[8];

		for (var i = 0; i < 8; i++) {
			cube[i] = GetTerrainDataAtPoint (position + GameData.CornerTable[i]);
		}

		// Get the configuration index of this cube.
		int configIndex = GetCubeConfig (cube);

		// ignore if inside or completely outside of terrain
		if (configIndex == 0 || configIndex == 255)
			return;

		// Loop through the triangles - 5 triangles per cube and three verts per triangle
		int edgeIndex = 0;
		for (int i = 0; i < 5; i++) {
			for (int v = 0; v < 3; v++) {

				// Get the current index. We increment triangleIndex through each loop.
				int index = GameData.TriangleTable[configIndex, edgeIndex];

				if (index == -1)
					// No more index beyond this
					return;

				// Get the vertices for the start and end of this edge.
				Vector3 vert1 = position + GameData.CornerTable[GameData.EdgeIndexes[index, 0]];
				Vector3 vert2 = position + GameData.CornerTable[GameData.EdgeIndexes[index, 1]];

				Vector3 vertPosition;
				if (isSmooth) {
					//get terrain data at the both ends of edge
					float vert1Value = cube[GameData.EdgeIndexes[index, 0]];
					float vert2Value = cube[GameData.EdgeIndexes[index, 1]];

					//Calculate difference in values to find where the terrain height intersects the edge of the cube
					float diff = vert2Value - vert1Value;

					if (diff == 0) { // zero indicates that the intersect is in the middle
						diff = terrainSurface;
					} else {
						diff = (terrainSurface - vert1Value) / diff;
					}

					//calculate actual intersect
					vertPosition = vert1 + ((vert2 - vert1) * diff);

				} else {
					// Get the midpoint of this edge.
					vertPosition = (vert1 + vert2) / 2f;
				}

				// Add to our vertices and triangles list and increment the edgeIndex.
				if (reduceVerts) {
					triangles.Add (SearchForMatchingVertices (vertPosition));
				} else {

					vertices.Add (vertPosition);
					triangles.Add (vertices.Count - 1);
				}
				edgeIndex++;

				//Populate Objects Here
			}
		}
	}

	float GetTerrainDataAtPoint (Vector3Int point) {
		return terrainMap[point.x, point.y, point.z];
	}

	void ClearMeshData () {
		vertices.Clear ();
		triangles.Clear ();
	}

	int SearchForMatchingVertices (Vector3 vertex) {
		if (vertices.Contains (vertex)) return vertices.IndexOf (vertex);

		//if new vert is unique, add it to the list and return the last index
		vertices.Add (vertex);
		return vertices.Count - 1;
	}

	void BuildMesh () {

		Mesh mesh = new Mesh ();
		mesh.vertices = vertices.ToArray ();
		mesh.triangles = triangles.ToArray ();

		//set up Uvs

		Vector2[] uvs = new Vector2[mesh.vertices.Length];

		for (var i = 0; i < uvs.Length; i++) {
			uvs[i] = new Vector2 (vertices[i].x, vertices[i].z);
		}
		mesh.uv = uvs;

		// for (int i = 0, z = 0; z <= width; z++) {
		// 	for (int x = 0; x <= width; x++) {
		// 		uvs[i] = new Vector2 ((float) x / width, (float) z / width);
		// 		i++;
		// 	}
		// }
		// mesh.uv = uvs;

		mesh.RecalculateNormals ();
		meshFilter.mesh = mesh;
		meshCollider.sharedMesh = mesh;
	}

	/*
		Terrain Manipulation
	*/

	public void PlaceTerrain (Vector3 pos) {
		Vector3Int v3IntPos = new Vector3Int (Mathf.CeilToInt (pos.x), Mathf.CeilToInt (pos.y), Mathf.CeilToInt (pos.z));
		terrainMap[v3IntPos.x, v3IntPos.y, v3IntPos.z] = 0f;
		CreateMeshData ();
	}

	public void RemoveTerrain (Vector3 pos) {
		Vector3Int v3IntPos = new Vector3Int (Mathf.FloorToInt (pos.x), Mathf.FloorToInt (pos.y), Mathf.FloorToInt (pos.z));
		terrainMap[v3IntPos.x, v3IntPos.y, v3IntPos.z] = 1f;
		CreateMeshData ();

	}

}