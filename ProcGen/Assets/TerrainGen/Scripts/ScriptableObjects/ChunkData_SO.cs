using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "ChunkData_SO", menuName = "ProcGen/Terrain Data/ChunkData_SO", order = 0)]
public class ChunkData_SO : ScriptableObject {
    public bool _isSmooth;

    [Header ("Reduce Verts for Smooth Shading")]
    public bool _reduceVerts;
    public bool _useRandomSeed;
    public string _seed = "24.001";
    public Material _materialToUse;

    // [Header ("ObjectsToSpawn")]
    // public List<GameObject> trees = new List<GameObject> ();
}