using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Color colourAlive;
    [SerializeField] Color colourDead;

    [Header ("Debug")]
    public Vector2Int coordinate;
    public List<Cell> neighbourhood;

    public bool alive = false;

    void FLip () {
        alive = !alive;

        spriteRenderer.color = alive? colourAlive : colourDead;
    }
    internal void SetState (bool state) {
        if (alive != state) FLip ();
    }

    internal void SetCoordinate (Vector2Int coordinate) {
        this.coordinate = coordinate;
    }

    internal void SetNeighbourhood (List<Cell> _neighbourhood) {
        this.neighbourhood = _neighbourhood;
    }

    internal void SetNextGeneration (bool _alive) {
        StartCoroutine(ApplyNExtGeneration(_alive));
    }

    IEnumerator ApplyNExtGeneration (bool _state) {
        yield return new WaitForEndOfFrame();
        SetState (_state);
    }
}