using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using UnityEngine.Events;

public class GenerateLSystem : MonoBehaviour {

    [Header ("References")]
    [SerializeField] Transform turtle;
    [SerializeField] LineRendererObjectPool turtlePond;
    new Camera camera;

    [Header ("L-System")]
    [SerializeField] string axiom;
    [SerializeField] List<Grammar> grammars = new List<Grammar> ();

    [Header ("Settings")]
    [SerializeField] int generations = 3;
    [SerializeField] float lineLength = 0.5f;
    [SerializeField] float rotationAngle = 5f;
    [SerializeField, Range (0, 10)] float frameWait = 0;
    [SerializeField, Range (0, 10)] int stepsPerFrame = 1;

    [Header ("Output")]
    [SerializeField, TextArea (3, 10)] string sequence;
    [SerializeField] bool drawingSequence = false;

    private void Start () {
        camera = Camera.main;
    }

    [Button]
    public void Generate () {
        drawingSequence = true;
        StartCoroutine (GenerateSequence (() => {
            StartCoroutine (DrawSequence (sequence));
        }));

    }

    [Button]
    public void StopGenerate () {
        drawingSequence = false;
    }

    IEnumerator GenerateSequence (UnityAction OnComplete) {
        sequence = axiom;

        for (var i = 0; i < generations; i++) {
            sequence = ProcessSequence (sequence);
            yield return new WaitForSeconds (0.3f);
        }

        yield return null;
        OnComplete.Invoke ();
    }

    string ProcessSequence (string sequence) {
        string processedSequence = string.Empty;
        char[] characters = sequence.ToCharArray ();

        for (var i = 0; i < characters.Length; i++) {
            bool found = false;
            for (var j = 0; j < grammars.Count; j++) {
                if (characters[i] == grammars[j].variable) {
                    processedSequence += grammars[j].rule;

                    found = true;
                    break;
                }
            }
            if (!found) {
                processedSequence += characters[i];
            }
        }
        return processedSequence;
    }

    IEnumerator DrawSequence (string sequence) {
        int stepCount = 0;

        char[] characters = sequence.ToCharArray ();

        turtle.localPosition = Vector3.zero;
        turtle.localEulerAngles = Vector3.zero;

        List<LSystemLine> lines = new List<LSystemLine> ();
        LSystemLine line = turtlePond.GetFromObjectPool ();
        lines.Add (line);

        for (var i = 0; i < characters.Length; i++) {

            LSystem_Koch (characters[i]);

            line = lines[lines.Count - 1];
            line.lineRenderer.positionCount++;
            line.lineRenderer.SetPosition (line.lineRenderer.positionCount - 1, turtle.localPosition);

            //tweak camera
            if (turtle.localPosition.y > camera.orthographicSize * 2) {
                camera.orthographicSize = ((int) turtle.localPosition.y / 2) + 1;
                transform.position = new Vector3 (0, -camera.orthographicSize, 0);
            }
            stepCount++;
            if (stepCount >= stepsPerFrame) {
                stepCount = 0;

                yield return new WaitForSeconds (frameWait);
            }

            if (!drawingSequence) break;
        }
        while (drawingSequence) {
            yield return null;
        }

        if (!drawingSequence && lines.Count > 0) {
            for (var i = 0; i < lines.Count; i++) {
                
                turtlePond.PutIntoObjectPool (lines[i]);
            }
        }

        yield return null;
    }

    private void LSystem_Koch (char _char) {
        switch (_char) {
            case 'F':
                Debug.Log ($"forward");
                turtle.Translate (Vector3.up * lineLength);
                break;
            case '+':
                Debug.Log ($"Turn Left");
                turtle.Rotate (Vector3.forward * -rotationAngle);
                // turtle.localEulerAngles -= new Vector3 (0, 0, rotationAngle);
                break;
            case '-':
            case '−':
                Debug.Log ($"Turn Right");
                turtle.Rotate (Vector3.forward * rotationAngle);
                break;
            default:
                Debug.LogError ($"character not found {_char}");
                break;
        }

    }
}

[System.Serializable]
public class Grammar {
    public char variable;
    public string rule;
}