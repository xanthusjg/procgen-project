using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSPDungeonRoom : MonoBehaviour {

    [SerializeField] GameObject finalRoom;
    [SerializeField] MeshRenderer outlineRenderer;
    Vector3 originalPosition = new Vector3 (0.25f, 0f, 0.025f);

    internal void DisableOutline (){

    }

    internal void GenerateFinalRoom () {
        finalRoom.SetActive (true);
        finalRoom.transform.localPosition += new Vector3 () {
            x = Random.Range (-0.25f, 0.25f),
            y = 0.0f,
            z = Random.Range (-0.25f, 0.25f),

        };
    }

    private void OnDisable () {
        finalRoom.SetActive (false);
        transform.transform.localPosition = originalPosition;
    }
}