using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class BSPDungeonGen : MonoBehaviour {

    [Header ("Parameters")]
    [SerializeField] Vector2Int mapSize;
    [SerializeField] Vector2 minRoomSize;
    [SerializeField] int generationDepth = 3;
    [SerializeField] int seed;

    [Header ("References")]
    [SerializeField] PoolBSPDungeonRoom poolBSPDungeonRooms;

    [Header ("Diagnostics")]
    [SerializeField] Partition rootPartition;
    [SerializeField] List<BSPDungeonRoom> rooms = new List<BSPDungeonRoom> ();
    [SerializeField] List<BSPDungeonRoom> finalRooms = new List<BSPDungeonRoom> ();

    [Button]
    void Generate () {
        ClearRooms ();
        rootPartition = new Partition ();
        rootPartition.partitionSize = new Rect (0, 0, mapSize.x, mapSize.y);

        Partition currentPartition = rootPartition;
        currentPartition.room = SpawnRoom (rootPartition.partitionSize, transform);

        List<Partition> childPartitions = new List<Partition> ();
        List<Partition> nextPartitions = new List<Partition> ();

        childPartitions.Add (currentPartition);

        Random.InitState (seed);
        bool splitHorizontally = Random.Range (0, 2) == 0 ? true : false;

        for (var i = 0; i < generationDepth; i++) {
            for (var j = 0; j < childPartitions.Count; j++) {
                currentPartition = childPartitions[j];
                splitHorizontally = !splitHorizontally;

                Rect a_ = new Rect ();
                Rect b_ = new Rect ();

                if (splitHorizontally) {
                    float splitPos = (currentPartition.partitionSize.height / 2) + Random.Range (-currentPartition.partitionSize.height / 4, currentPartition.partitionSize.height / 4);
                    SplitHorizontally (currentPartition, splitPos, out a_, out b_);
                } else {
                    float splitPos = (currentPartition.partitionSize.width / 2) + Random.Range (-currentPartition.partitionSize.width / 4, currentPartition.partitionSize.width / 4);
                    SplitVertically (currentPartition, splitPos, out a_, out b_);

                }

                if (CheckRoomSize (a_) && CheckRoomSize (b_) && i < generationDepth - 1) {
                    //More Partitions to come
                    CreatePartition (currentPartition, nextPartitions, a_);
                    CreatePartition (currentPartition, nextPartitions, b_);

                    // Partition b_Partition = new Partition ();
                    // b_Partition.partitionSize = b_;
                } else {
                    //Final Room
                    finalRooms.Add (currentPartition.room);
                    currentPartition.room.GenerateFinalRoom ();
                }
            }
        }

        childPartitions = nextPartitions;
        nextPartitions = new List<Partition> ();

    }

    private void CreatePartition (Partition currentPartition, List<Partition> nextPartitions, Rect rect) {
        Partition newPartition = new Partition ();
        newPartition.partitionSize = rect;
        newPartition.room = SpawnRoom (newPartition.partitionSize, currentPartition.room.transform);
        nextPartitions.Add (newPartition);
        currentPartition.childPartitions.Add (newPartition);
    }

    private bool CheckRoomSize (Rect rect) {
        return rect.width >= minRoomSize.x && rect.height >= minRoomSize.y;
    }

    void SplitVertically (Partition currentPartition, float splitPos, out Rect left, out Rect right) {
        left = new Rect () {
            x = currentPartition.partitionSize.x,
            y = currentPartition.partitionSize.y,
            width = splitPos,
            height = currentPartition.partitionSize.height
        };

        right = new Rect () {
            x = currentPartition.partitionSize.x + splitPos,
            y = currentPartition.partitionSize.y,
            width = currentPartition.partitionSize.width - splitPos,
            height = currentPartition.partitionSize.height
        };
    }

    void SplitHorizontally (Partition currentPartition, float splitPos, out Rect bottom, out Rect top) {
        bottom = new Rect () {
            x = currentPartition.partitionSize.x,
            y = currentPartition.partitionSize.y,
            width = currentPartition.partitionSize.width,
            height = splitPos
        };

        top = new Rect () {
            x = currentPartition.partitionSize.x,
            y = currentPartition.partitionSize.y + splitPos,
            width = currentPartition.partitionSize.width,
            height = currentPartition.partitionSize.height - splitPos
        };
    }

    private void ClearRooms () {
        for (var i = 0; i < rooms.Count; i++) {
            poolBSPDungeonRooms.PutIntoObjectPool (rooms[i]);

        }
        rooms.Clear ();
    }

    private BSPDungeonRoom SpawnRoom (Rect partitionSize, Transform roomParent) {

        BSPDungeonRoom newRoom = poolBSPDungeonRooms.GetFromObjectPool ();
        newRoom.transform.localScale = new Vector3 (partitionSize.width, 1, partitionSize.height);
        newRoom.transform.localPosition = new Vector3 (partitionSize.x, 0, partitionSize.y) + roomParent.position;
        newRoom.transform.SetParent (roomParent, true);
        rooms.Add (newRoom);

        return newRoom;
    }
}

[System.Serializable]
public class Partition {
    public Rect partitionSize;
    public List<Partition> childPartitions = new List<Partition> ();
    public BSPDungeonRoom room;
}