using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrunkenWalk : MonoBehaviour {

    [SerializeField] RawImage generatedOutput;
    [SerializeField] Color32 backgroundColour;
    [SerializeField] Color32 lineColour;
    [SerializeField, Range (0.01f, 1f)] float resolutionScale = 1;
    [SerializeField] int paddingPixels = 2;
    [SerializeField] int pixelsPerFrame = 1;

    bool generating = false;

    public void Generate () {
        generating = !generating;
        if (generating) {
            Debug.Log ($"start generating");
            StartCoroutine (Walking ());
        } else {
            Debug.Log ($"stop generating");
        }
    }

    IEnumerator Walking () {
        Texture2D texture = new Texture2D ((int) (Screen.width * resolutionScale), (int) (Screen.height * resolutionScale), TextureFormat.RGBA32, true);
        Vector2Int point = new Vector2Int (texture.width / 2, texture.height / 2);
        texture.filterMode = FilterMode.Point;

        FillTexture (backgroundColour, ref texture);

        while (generating) {
            StepAndWalk (ref texture, ref point);
            generatedOutput.texture = texture;
            yield return null;

        }
        Texture2D finalTexture = new Texture2D (texture.width, texture.height, TextureFormat.RGBA32, true);
        FillTexture (backgroundColour, ref finalTexture);
        finalTexture.filterMode = FilterMode.Point;
        AddPadding (ref texture, ref finalTexture);
        generatedOutput.texture = finalTexture;
    }

    void FillTexture (Color32 fillColour, ref Texture2D texture) {
        for (int w = 0; w < texture.width; w++) {
            for (int h = 0; h < texture.height; h++) {
                texture.SetPixel (w, h, fillColour);
            }
        }
        texture.Apply ();
    }

    void StepAndWalk (ref Texture2D texture, ref Vector2Int point) {
        for (var i = 0; i < pixelsPerFrame; i++) {
            bool sidestep = Random.Range (0, 2) == 0 ? false : true;

            if (sidestep) { // horizontal
                point.x += Random.Range (0, 2) == 0 ? -1 : 1;
            } else { // vertical
                point.y += Random.Range (0, 2) == 0 ? -1 : 1;

            }

            texture.SetPixel (point.x, point.y, lineColour);
            texture.Apply ();

        }

    }

    void AddPadding (ref Texture2D texture, ref Texture2D finalTexture) {
        for (int w = 0; w < texture.width; w++) {
            for (int h = 0; h < texture.height; h++) {
                Color32 pixelColour = texture.GetPixel (w, h);
                if ((Color) pixelColour != backgroundColour) {
                    finalTexture.SetPixel (w, h, pixelColour);

                    for (int p = 0; p < paddingPixels; p++) {
                        finalTexture.SetPixel (w, h + p, pixelColour); //North
                        finalTexture.SetPixel (w, h - p, pixelColour); //South
                        finalTexture.SetPixel (w + p, h, pixelColour); //East
                        finalTexture.SetPixel (w - p, h, pixelColour); //West

                        for (int d = 0; d < paddingPixels; d++) {
                            finalTexture.SetPixel (w + d, h + p, pixelColour);
                            finalTexture.SetPixel (w - d, h + p, pixelColour);
                            finalTexture.SetPixel (w + p, h - d, pixelColour);
                            finalTexture.SetPixel (w + p, h + d, pixelColour);
                        }
                    }
                }
            }
        }
        finalTexture.Apply ();
    }
}