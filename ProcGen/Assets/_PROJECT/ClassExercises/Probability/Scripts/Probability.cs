using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probability : MonoBehaviour {

    [SerializeField] List<EnemyProbabilityValue> enemyProbabilityValues = new List<EnemyProbabilityValue> ();

    [Header ("Debug")]
    [SerializeField] float totalProbability;

    [EasyButtons.Button]
    void RollNumber () {
        totalProbability = 0;

        enemyProbabilityValues.ForEach (x => {
            totalProbability += x.probability + x.probabilityLikelinessOffset;
        });

        float randomNum = Random.Range (0, totalProbability);

        EnemyProbabilityValue selectedValue = null;
        bool foundCandidate = false;
        float runningTotal = 0;

        for (var i = 0; i < enemyProbabilityValues.Count; i++) {
            bool offsetValue = true;
            if (!foundCandidate) {
                runningTotal += enemyProbabilityValues[i].probability + enemyProbabilityValues[i].probabilityLikelinessOffset;

                if (randomNum <= runningTotal) {
                    selectedValue = enemyProbabilityValues[i];
                    foundCandidate = true;
                    offsetValue = false;

                    enemyProbabilityValues[i].probabilityLikelinessOffset = 0;
                    enemyProbabilityValues[i].rollCount++;

                    string colour = selectedValue.enemyDifficulty == EnemyDifficulty.Easy? "green": selectedValue.enemyDifficulty == EnemyDifficulty.Medium? "yellow": "red";

                    Debug.Log ($"Rolled {randomNum} / {runningTotal} and found <color='{colour}'> {selectedValue}</color> enemy");
                }
            }
            if (offsetValue) {
                enemyProbabilityValues[i].probabilityLikelinessOffset += enemyProbabilityValues[i].probabilityIncrement;
            }
        }

    }

}

public enum EnemyDifficulty {
    Easy,
    Medium,
    Hard
}

[System.Serializable]
public class EnemyProbabilityValue {
    // [Header ("Settings")]
    public EnemyDifficulty enemyDifficulty;
    public float probability;
    public float probabilityIncrement = 1.0f;

    // [Header ("Debug")]
    public float probabilityLikelinessOffset;
    public int rollCount;
}