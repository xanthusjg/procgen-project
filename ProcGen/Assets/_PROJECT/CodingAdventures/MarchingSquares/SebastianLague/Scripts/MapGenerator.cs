using System;
using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    [Header ("Map Params")]
    [SerializeField] int width;
    [SerializeField] int height;
    [SerializeField][Range (0, 100)] int randomFillPercent;

    [Header ("Seed Settings")]
    [SerializeField] bool useRandomSeed;
    [SerializeField] string seed;

    [Header ("Map Smoothing Settings")]
    [SerializeField] int smoothPasses = 5;

    [Header ("Refs")]
    [SerializeField] MeshGenerator meshGen;

    /*
        Private Variables
    */
    int[, ] map;

    private void Start () {
        GenerateMap ();
    }

    [Button]
    private void GenerateMap () {
        map = new int[width, height];
        RandomFIllMap ();

        for (var i = 0; i < smoothPasses; i++) {
            SmoothMap ();
        }
        meshGen.GenerateMesh (map, 1);
    }

    void RandomFIllMap () {
        //Seed Gen or use
        if (useRandomSeed) {
            seed = Time.time.ToString ();
        }
        System.Random rng = new System.Random (seed.GetHashCode ());

        //loop through tiles in map
        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
                    map[x, y] = 1;
                } else {
                    map[x, y] = (rng.Next (0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap () {
        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                int neighbourWallTiles = GetSurroundingWallCount (x, y);

                if (neighbourWallTiles > 4) {
                    map[x, y] = 1;
                } else if (neighbourWallTiles < 4) {
                    map[x, y] = 0;
                }
            }
        }
    }

    int GetSurroundingWallCount (int gridX, int gridY) {
        int wallCount = 0;
        for (var neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++) {
            for (var neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++) {
                //check if neighbour is inside the map
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height) {
                    if (neighbourX != gridX || neighbourY != gridY) {
                        wallCount += map[neighbourX, neighbourY];
                    }
                } else {
                    wallCount++;
                }
            }

        }

        return wallCount;
    }
    /*
    private void OnDrawGizmos () {
        if (map != null) {
            for (var x = 0; x < width; x++) {
                for (var y = 0; y < height; y++) {
                    Gizmos.color = (map[x, y] == 1) ? Color.black : Color.white;
                    Vector3 pos = new Vector3 (-width / 2 + x + 0.5f, 0, -height / 2 + y + 0.5f);
                    Gizmos.DrawCube (pos, Vector3.one);
                }
            }

        }
    }
    */
}