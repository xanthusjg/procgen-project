# ProcGen Project - Terrain Generation

## Content
- Project Purpose
- Intended Senario for Use
- How To Use & Feature Breakdown
- Limitations
- Requirements

### Project Purpose

The purpose of this tool is to create procedurally generated landmasses and sculpting thereof. 

### Intended Senario for Use

Senario for use is for the generation of large landmasses, in order to make it easier to create worlds. 

### How To Use & Feature Breakdown

- Drag in the WorldGenerator Prefab into your scene.
- In the inspector, you can edit the parameters of the terrain that you want to generate. 

![](Images/Inspector/WordGeneratorInspectorSettings.png)

- World Size indicates the number of chunks that the geneartor will generate - this size is used for the lenght and width of the generated landmass.
- Is Smooth will toggle between smooth terrain and a more blocky looking terrain.
- Reduce verts, when toggled on, will give the terrain a more smooth-shaded look. It is recomended to only use this when Is Smooth is on.
- Seed settings allow you to change the seed of the genearted terrain.
- The material reference is applied to the generated terrain.
- The generate button will generate the terrain - Only in Play mode.

There is an example set up in the Demo folder for reference.

### Limitations

- Large numbers of chunks take much longer to process.
- Smoothing larger amounts of chunks takes much longer to process.
- The terraforming tool does not currently work.
- The terrain is only generated through perlin noise and no other options are available.
- Only basic materials are supported at thew moment.

### Requirements

- [Easy Buttons] (https://github.com/madsbangh/EasyButtons)
- [Free Trees] (https://assetstore.unity.com/packages/3d/vegetation/trees/free-trees-103208)
